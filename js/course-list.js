/**
 * iOS Frontend/js/course-list.js
 *
 * Worked on by: Silvery Fu (Team 1, Group A)
 *   2014-07-20: Added dynamic routing for viewing course feature
 *   2014-07-23: Fixed button redirect for integration
 */

$(function() {
  var url_base = Accounts.getCookie('url_base')
    , courses = Accounts.getCourses()
    , clist$ = $('#clist')
    , hback$ = $('#hback')
    , cback$ = $('#cback');

  /* Route to the material page */
  Accounts.getCourses().then(function(data) {
    $.each(data, function(index, course){
      console.log(course);
      clist$.append(('<li ><a href="#course_page?'+course.id+
                    '" class="ui-btn ui-shadow" data-transition="slide">'+
                    course.name+'</a></li>'));
    });
  });

  clist$.enhanceWithin();
  
  /* Define the dynamic router */
  var course_list = {};
  
  course_list.app = {

    initialize: function() {
      document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    
    /* Setting the course id in the cookie */
    postBeforeShow: function(event, args) {
      Accounts.setCookie('current_course_id', args[1]);
      $('#course_info').html("<h2> Course id:"+args[1]+"</h2>");
      $('#course_info').enhanceWithin();
    },
    
    onDeviceReady: function() {
      FastClick.attach(document.body);
    }
  };
  
  /* Handle the routing event for all tags with id course_page?[number] */
  course_list.router = new $.mobile.Router(
    {
      "#course_page[?](\\d+)": {handler: "postBeforeShow", events:"bs"}
    },
    course_list.app
  );

  cback$.on("click", function(){
    Accounts.removeCookie('current_course_id');
  });

  $('#attbtn').click(function(){
    window.location.href = './attendance.html';
  });

  $('#quizbtn').click(function(){
    console.log('quiz');
    window.location.href = './quiz.html';
  });
  
  hback$.click(function(){
    window.location.href = "../index.html";
  })
});
